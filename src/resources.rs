use std::sync::Arc;

use waef_core::ResourceStore;

pub struct WasmResourceStore {
}
impl WasmResourceStore {
    pub fn create() -> Arc<dyn ResourceStore> {
        Arc::new(Self { })
    }
}
impl ResourceStore for WasmResourceStore {
    fn get_size(&self, key: u128) -> Option<usize> {
        crate::bindgen::resources::get_size(key)
    }

    fn remove(&self, key: u128) -> Option<Box<[u8]>> {
        crate::bindgen::resources::remove(key)
    }

    fn put_slice(&self, key: u128, start_index: usize, data: &[u8]) {
        crate::bindgen::resources::put_slice(key, start_index, data);
    }

    fn get_slice(&self, key: u128, start_index: usize, buffer: &mut [u8]) -> Option<usize> {
        crate::bindgen::resources::get_slice(key, start_index, buffer)
    }

    fn get(&self, key: u128, buffer: &mut [u8]) -> Option<usize> {
        crate::bindgen::resources::get(key, buffer)
    }

    fn put(&self, key: u128, data: &[u8]) {
        crate::bindgen::resources::put(key, data);
    }
    
    fn get_cloned(&self, key: u128) -> Option<Vec<u8>> {
        crate::bindgen::resources::get_cloned(key)
    }
}
