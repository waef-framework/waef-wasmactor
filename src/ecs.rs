use waef_core::ecs::EcsRegistry;
use std::{collections::HashMap, sync::Arc};
use waef_core::core::WaefError;

pub struct WasmEcsRegistry {
}
impl WasmEcsRegistry {
    pub fn create() -> Arc<dyn EcsRegistry> {
        Arc::new(Self { })
    }
}
impl EcsRegistry for WasmEcsRegistry {
    fn list_component_definitions(&self) -> HashMap<String, usize> {
        crate::bindgen::ecs::list_component_definitions().into_iter().collect()
    }

    fn define_component_by_size(&self, name: String, size: usize) -> Result<(), WaefError> {
        crate::bindgen::ecs::define_component(&name, size);
        Ok(())
    }

    fn get_component_size(&self, name: String) -> Option<usize> {
        Some(crate::bindgen::ecs::get_component_size(&name))
    }

    fn create_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        crate::bindgen::ecs::create_entity(entity_id);
        Ok(())
    }

    fn destroy_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        crate::bindgen::ecs::destroy_entity(entity_id);
        Ok(())
    }

    fn attach_component_buffer(
        &self,
        component_name: String,
        entity_id: u128,
        component_buffer: Box<[u8]>,
    ) -> Result<(), WaefError> {
        crate::bindgen::ecs::attach_component(entity_id, &component_name, &component_buffer);
        Ok(())
    }

    fn detach_component_by_name(
        &self,
        component_name: String,
        entity_id: u128,
    ) -> Result<(), WaefError> {
        crate::bindgen::ecs::detach_component(entity_id, &component_name);
        Ok(())
    }

    fn apply(&self, component_query: &[String], callback: &mut dyn FnMut(u128, &mut [&mut [u8]])) {
        let component_query: Vec<&str> = component_query.iter().map(|m| m.as_str()).collect();
        crate::bindgen::ecs::apply(&component_query[..], callback);
    }

    fn apply_set(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let component_query: Vec<&str> = component_query.iter().map(|m| m.as_str()).collect();
        crate::bindgen::ecs::apply_set(entity_ids, &component_query[..], callback);
    }

    fn apply_single(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let component_query: Vec<&str> = component_query.iter().map(|m| m.as_str()).collect();
        crate::bindgen::ecs::apply_single(entity_id, &component_query[..], callback);
    }

    fn apply_set_unsafe(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let component_query: Vec<&str> = component_query.iter().map(|m| m.as_str()).collect();
        crate::bindgen::ecs::apply_set_unsafe(entity_ids, &component_query[..], callback);
    }

    fn apply_unsafe(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let component_query: Vec<&str> = component_query.iter().map(|m| m.as_str()).collect();
        crate::bindgen::ecs::apply_unsafe(&component_query[..], callback);
    }

    fn apply_single_unsafe(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let component_query: Vec<&str> = component_query.iter().map(|m| m.as_str()).collect();
        crate::bindgen::ecs::apply_single_unsafe(entity_id, &component_query[..], callback);
    }
}
