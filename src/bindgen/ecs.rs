use std::{
    ffi::{c_char, c_void, CString},
    io::{Read, Write},
};
use waef_core::util::pod::Pod;

mod wasm {
    use std::ffi::{c_char, c_void};

    #[allow(dead_code)]
    #[link(wasm_import_module = "waef.ecs")]
    extern "C" {
        pub fn create_entity(high: u64, low: u64);
        pub fn destroy_entity(high: u64, low: u64);

        pub fn define_component(name: *const c_char, size: u32);
        pub fn list_component_definitions() -> u32;
        pub fn get_component_size(name: *const c_char) -> u32;

        pub fn attach_component(
            high: u64,
            low: u64,
            name: *const c_char,
            data: *const c_void,
            data_size: u32,
        );
        pub fn detach_component(high: u64, low: u64, name: *const c_char);
        pub fn apply(
            buffer: *mut c_void,
            buffer_size: u32,
            buffer_alignment: u32,
            component_names_ptr: *const *const c_char,
            component_names_count: u32,
            callback: extern "C" fn(*mut c_void, u64, u64, *mut c_void),
            context: *mut c_void,
        );
        pub fn apply_single(
            high: u64,
            low: u64,
            buffer: *mut c_void,
            buffer_size: u32,
            buffer_alignment: u32,
            component_names_ptr: *const *const c_char,
            component_names_count: u32,
            callback: extern "C" fn(*mut c_void, u64, u64, *mut c_void),
            context: *mut c_void,
        );
        pub fn apply_set(
            entity_ids: *const c_void,
            entity_id_count: u32,
            buffer: *mut c_void,
            buffer_size: u32,
            buffer_alignment: u32,
            component_names_ptr: *const *const c_char,
            component_names_count: u32,
            callback: extern "C" fn(*mut c_void, u64, u64, *mut c_void),
            context: *mut c_void,
        );
    }

    pub fn u128_to_high_low(value: u128) -> (u64, u64) {
        ((value >> 64) as u64, (value as u64))
    }

    pub fn high_low_to_u128(high: u64, low: u64) -> u128 {
        ((high as u128) << 64) | (low as u128)
    }
    pub extern "C" fn apply_callback<F: FnMut(u128, &mut [&mut [u8]])>(
        context: *mut c_void,
        high: u64,
        low: u64,
        buffer: *mut c_void,
    ) {
        let callback_ptr = context as *mut (usize, Vec<usize>, F);
        let callback = unsafe { &mut (*callback_ptr) };

        let entity_id = high_low_to_u128(high, low);

        let mut components = callback
            .1
            .iter()
            .fold(
                (0, Vec::with_capacity((*callback).1.len())),
                |(index, mut components), size| {
                    let component_data = unsafe { (buffer as *mut u8).add(index) };
                    let component =
                        unsafe { std::slice::from_raw_parts_mut(component_data, *size) };
                    components.push(component);
                    (index + size + ((8 - (size % 8)) % 8), components)
                },
            )
            .1;
        (callback.2)(entity_id, &mut components[..])
    }

    pub extern "C" fn apply_callback_unsafe<F: FnMut(u128, &[(*mut u8, usize)])>(
        context: *mut c_void,
        high: u64,
        low: u64,
        buffer: *mut c_void,
    ) {
        let callback_ptr = context as *mut (usize, Vec<usize>, F);
        let callback = unsafe { &mut (*callback_ptr) };

        let entity_id = high_low_to_u128(high, low);

        let mut components = callback
            .1
            .iter()
            .fold(
                (0, Vec::with_capacity((*callback).1.len())),
                |(index, mut components), size| {
                    let component_data = unsafe { (buffer as *mut u8).add(index) };
                    components.push((component_data, *size));
                    (index + size + ((8 - (size % 8)) % 8), components)
                },
            )
            .1;
        ((*callback).2)(entity_id, &mut components[..])
    }
}

pub(crate) fn create_entity(id: u128) {
    let (high, low) = wasm::u128_to_high_low(id);

    unsafe {
        wasm::create_entity(high, low);
    }
}

pub(crate) fn destroy_entity(id: u128) {
    let (high, low) = wasm::u128_to_high_low(id);

    unsafe {
        wasm::destroy_entity(high, low);
    }
}

pub(crate) fn define_component(name: &str, size: usize) {
    let cname = CString::new(name).unwrap();
    unsafe {
        wasm::define_component(cname.as_ptr(), size as u32);
    }
}

pub(crate) fn list_component_definitions() -> Vec<(String, usize)> {
    let buffer_size = unsafe { wasm::list_component_definitions() } as usize;
    let mut buffer = vec![0u8; buffer_size];

    if let Err(err) = std::io::stdin().read_exact(buffer.as_mut_slice()) {
        _ = std::io::stderr().write_all(
            format!("Failed to read buffer for list_component_definitions: {err}").as_bytes(),
        );
    }

    let mut result = Vec::new();
    let mut head = 0;
    let mut index = 0;
    while head < buffer.len() {
        while index < buffer.len() && buffer[index] != 0 {
            index = index + 1;
        }
        if index + 4 >= buffer.len() {
            _ = std::io::stderr().write_all(
                format!("Error reading buffer for list_component_definitions: early end of buffer")
                    .as_bytes(),
            );
            return result;
        }
        let component_name = unsafe { String::from_utf8_unchecked(buffer[head..index].to_vec()) };
        index = index + 1;

        let component_size = u32::from_bytes(&buffer[index..index + 4]);
        index = index + 4;

        result.push((component_name, component_size as usize));
        head = index;
    }
    result
}

pub(crate) fn get_component_size(component_name: &str) -> usize {
    let cname = CString::new(component_name).unwrap();

    unsafe { wasm::get_component_size(cname.as_ptr()) as usize }
}

pub(crate) fn attach_component(entity_id: u128, component_name: &str, data: &[u8]) {
    let cname = CString::new(component_name).unwrap();

    let (high, low) = wasm::u128_to_high_low(entity_id);
    unsafe {
        wasm::attach_component(
            high,
            low,
            cname.as_ptr(),
            data.as_ptr() as *const c_void,
            data.len() as u32,
        )
    }
}

pub(crate) fn detach_component(entity_id: u128, component_name: &str) {
    let cname = CString::new(component_name).unwrap();
    let (high, low) = wasm::u128_to_high_low(entity_id);
    unsafe { wasm::detach_component(high, low, cname.as_ptr()) }
}

fn calculate_component_sizes_and_buffer(component_names: &[&str]) -> (Vec<u8>, Vec<usize>) {
    let component_sizes: Vec<usize> = component_names
        .iter()
        .flat_map(|name| match name.chars().nth(0) {
            Some('?') => Some(get_component_size(&name[1..])),
            Some('!') => None,
            _ => Some(get_component_size(name)),
        })
        .collect();

    let total_size = component_sizes
        .iter()
        .fold(0, |a, b| a + b + ((8 - (b % 8)) % 8));
    let buffer = vec![0u8; total_size];

    (buffer, component_sizes)
}

pub(crate) fn apply<F: FnMut(u128, &mut [&mut [u8]])>(component_names: &[&str], callback: F) {
    let (mut buffer, component_sizes) = calculate_component_sizes_and_buffer(component_names);
    let component_names_store: Vec<CString> = component_names
        .iter()
        .map(|n| CString::new(*n).unwrap())
        .collect();

    let component_names_arr: Vec<*const c_char> =
        component_names_store.iter().map(|n| n.as_ptr()).collect();
    let component_names_arr = component_names_arr.into_boxed_slice();

    let mut context_data = (buffer.len(), component_sizes, callback);
    let callback_ptr: *mut _ = &mut context_data;

    unsafe {
        wasm::apply(
            buffer.as_mut_slice().as_mut_ptr() as *mut c_void,
            buffer.len() as u32,
            8,
            component_names_arr.as_ptr(),
            component_names.len() as u32,
            wasm::apply_callback::<F>,
            callback_ptr as *mut c_void,
        )
    }
}

pub(crate) fn apply_unsafe<F: FnMut(u128, &[(*mut u8, usize)])>(
    component_names: &[&str],
    callback: F,
) {
    let (mut buffer, component_sizes) = calculate_component_sizes_and_buffer(component_names);

    let component_names_store: Vec<CString> = component_names
        .iter()
        .map(|n| CString::new(*n).unwrap())
        .collect();
    let component_names_arr: Vec<*const c_char> =
        component_names_store.iter().map(|n| n.as_ptr()).collect();
    let component_names_arr = component_names_arr.into_boxed_slice();

    let mut context_data = (buffer.len(), component_sizes, callback);
    let callback_ptr: *mut _ = &mut context_data;

    unsafe {
        wasm::apply(
            buffer.as_mut_slice().as_mut_ptr() as *mut c_void,
            buffer.len() as u32,
            8,
            component_names_arr.as_ptr(),
            component_names.len() as u32,
            wasm::apply_callback_unsafe::<F>,
            callback_ptr as *mut c_void,
        )
    }
}

pub(crate) fn apply_single<F: FnMut(u128, &mut [&mut [u8]])>(
    entity_id: u128,
    component_names: &[&str],
    callback: F,
) {
    let (mut buffer, component_sizes) = calculate_component_sizes_and_buffer(component_names);

    let words: [u64; 2] = unsafe { std::mem::transmute::<u128, [u64; 2]>(entity_id) };

    let component_names_store: Vec<CString> = component_names
        .iter()
        .map(|n| CString::new(*n).unwrap())
        .collect();
    let component_names_arr: Vec<*const c_char> =
        component_names_store.iter().map(|n| n.as_ptr()).collect();
    let component_names_arr = component_names_arr.into_boxed_slice();

    let mut context_data = (buffer.len(), component_sizes, callback);
    let callback_ptr: *mut _ = &mut context_data;

    unsafe {
        wasm::apply_single(
            words[1],
            words[0],
            buffer.as_mut_slice().as_mut_ptr() as *mut c_void,
            buffer.len() as u32,
            8,
            component_names_arr.as_ptr(),
            component_names.len() as u32,
            wasm::apply_callback::<F>,
            callback_ptr as *mut c_void,
        )
    }
}

pub(crate) fn apply_single_unsafe<F: FnMut(u128, &[(*mut u8, usize)])>(
    entity_id: u128,
    component_names: &[&str],
    callback: F,
) {
    let (mut buffer, component_sizes) = calculate_component_sizes_and_buffer(component_names);

    let words: [u64; 2] = unsafe { std::mem::transmute::<u128, [u64; 2]>(entity_id) };

    let component_names_store: Vec<CString> = component_names
        .iter()
        .map(|n| CString::new(*n).unwrap())
        .collect();
    let component_names_arr: Vec<*const c_char> =
        component_names_store.iter().map(|n| n.as_ptr()).collect();
    let component_names_arr = component_names_arr.into_boxed_slice();

    let mut context_data = (buffer.len(), component_sizes, callback);
    let callback_ptr: *mut _ = &mut context_data;

    unsafe {
        wasm::apply_single(
            words[1],
            words[0],
            buffer.as_mut_slice().as_mut_ptr() as *mut c_void,
            buffer.len() as u32,
            8,
            component_names_arr.as_ptr(),
            component_names.len() as u32,
            wasm::apply_callback_unsafe::<F>,
            callback_ptr as *mut c_void,
        )
    }
}

pub(crate) fn apply_set<F: FnMut(u128, &mut [&mut [u8]])>(
    entity_ids: &[u128],
    component_names: &[&str],
    callback: F,
) {
    let (mut buffer, component_sizes) = calculate_component_sizes_and_buffer(component_names);

    let component_names_store: Vec<CString> = component_names
        .iter()
        .map(|n| CString::new(*n).unwrap())
        .collect();
    let component_names_arr: Vec<*const c_char> =
        component_names_store.iter().map(|n| n.as_ptr()).collect();
    let component_names_arr = component_names_arr.into_boxed_slice();

    let mut context_data = (buffer.len(), component_sizes, callback);
    let callback_ptr: *mut _ = &mut context_data;

    unsafe {
        wasm::apply_set(
            entity_ids.as_ptr() as *const c_void,
            entity_ids.len() as u32,
            buffer.as_mut_slice().as_mut_ptr() as *mut c_void,
            buffer.len() as u32,
            8,
            component_names_arr.as_ptr(),
            component_names.len() as u32,
            wasm::apply_callback::<F>,
            callback_ptr as *mut c_void,
        )
    }
}

pub(crate) fn apply_set_unsafe<F: FnMut(u128, &[(*mut u8, usize)])>(
    entity_ids: &[u128],
    component_names: &[&str],
    callback: F,
) {
    let (mut buffer, component_sizes) = calculate_component_sizes_and_buffer(component_names);

    let component_names_store: Vec<CString> = component_names
        .iter()
        .map(|n| CString::new(*n).unwrap())
        .collect();
    let component_names_arr: Vec<*const c_char> =
        component_names_store.iter().map(|n| n.as_ptr()).collect();
    let component_names_arr = component_names_arr.into_boxed_slice();

    let mut context_data = (buffer.len(), component_sizes, callback);
    let callback_ptr: *mut _ = &mut context_data;

    unsafe {
        wasm::apply_set(
            entity_ids.as_ptr() as *const c_void,
            entity_ids.len() as u32,
            buffer.as_mut_slice().as_mut_ptr() as *mut c_void,
            buffer.len() as u32,
            8,
            component_names_arr.as_ptr(),
            component_names.len() as u32,
            wasm::apply_callback_unsafe::<F>,
            callback_ptr as *mut c_void,
        )
    }
}
