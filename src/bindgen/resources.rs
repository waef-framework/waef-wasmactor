use std::{
    io::{Read, Write},
    os::raw::c_void,
};

mod wasm {
    use std::ffi::c_void;

    #[allow(dead_code)]
    #[link(wasm_import_module = "waef.resources")]
    extern "C" {
        pub fn get_size(resource_key_high: u64, resource_key_low: u64) -> u32;
        pub fn get(
            resource_key_high: u64,
            resource_key_low: u64,
            target_ptr: *mut c_void,
            target_len: u32,
        ) -> u32;

        pub fn get_slice(
            resource_key_high: u64,
            resource_key_low: u64,
            offset_index: u32,
            target_ptr: *mut c_void,
            target_len: u32,
        ) -> u32;

        pub fn put(
            resource_key_high: u64,
            resource_key_low: u64,
            source_ptr: *const c_void,
            source_ptr_len: u32,
        ) -> u32;

        pub fn put_slice(
            resource_key_high: u64,
            resource_key_low: u64,
            target_index: u32,
            source_ptr: *const c_void,
            source_ptr_len: u32,
        ) -> u32;

        pub fn remove(resource_key_high: u64, resource_key_low: u64) -> u32;

        pub fn get_cloned(resource_key_high: u64, resource_key_low: u64) -> u32;
    }

    pub fn u128_to_high_low(value: u128) -> (u64, u64) {
        ((value >> 64) as u64, (value as u64))
    }
}

pub(crate) fn get_size(resource_id: u128) -> Option<usize> {
    let (high, low) = wasm::u128_to_high_low(resource_id);

    let result = unsafe { wasm::get_size(high, low) as usize };
    if result == 0 {
        None
    } else {
        Some(result)
    }
}

pub(crate) fn get(resource_id: u128, target: &mut [u8]) -> Option<usize> {
    let (high, low) = wasm::u128_to_high_low(resource_id);

    let result = unsafe {
        wasm::get(
            high,
            low,
            target.as_mut_ptr() as *mut c_void,
            target.len() as u32,
        ) as usize
    };
    if result == 0 {
        None
    } else {
        Some(result)
    }
}

pub(crate) fn get_slice(
    resource_id: u128,
    offset_index: usize,
    target: &mut [u8],
) -> Option<usize> {
    let (high, low) = wasm::u128_to_high_low(resource_id);

    let result = unsafe {
        wasm::get_slice(
            high,
            low,
            offset_index as u32,
            target.as_mut_ptr() as *mut c_void,
            target.len() as u32,
        ) as usize
    };
    if result == 0 {
        None
    } else {
        Some(result)
    }
}

pub(crate) fn put(resource_id: u128, source: &[u8]) -> usize {
    let (high, low) = wasm::u128_to_high_low(resource_id);

    unsafe {
        wasm::put(
            high,
            low,
            source.as_ptr() as *mut c_void,
            source.len() as u32,
        ) as usize
    }
}

pub(crate) fn put_slice(resource_id: u128, offset_index: usize, source: &[u8]) -> usize {
    let (high, low) = wasm::u128_to_high_low(resource_id);

    unsafe {
        wasm::put_slice(
            high,
            low,
            offset_index as u32,
            source.as_ptr() as *mut c_void,
            source.len() as u32,
        ) as usize
    }
}

pub(crate) fn remove(resource_id: u128) -> Option<Box<[u8]>> {
    let (high, low) = wasm::u128_to_high_low(resource_id);

    let size = unsafe { wasm::remove(high, low) as usize };
    if size == 0 {
        None
    } else {
        let mut stdin = std::io::stdin();
        let mut item_data = vec![0; size];
        if let Err(err) = stdin.read_exact(&mut item_data[..]) {
            _ = std::io::stderr()
                .write_all(format!("Failed to read removed resource: {err}").as_bytes());
            None
        } else {
            Some(item_data.into_boxed_slice())
        }
    }
}

pub(crate) fn get_cloned(resource_id: u128) -> Option<Vec<u8>> {
    let (high, low) = wasm::u128_to_high_low(resource_id);

    let size = unsafe { wasm::get_cloned(high, low) as usize };
    if size == 0 {
        None
    } else {
        let mut stdin = std::io::stdin();
        let mut item_data = vec![0; size];
        if let Err(err) = stdin.read_exact(&mut item_data[..]) {
            _ = std::io::stderr()
                .write_all(format!("Failed to read get_cloned resource: {err}").as_bytes());
            None
        } else {
            Some(item_data)
        }
    }
}
