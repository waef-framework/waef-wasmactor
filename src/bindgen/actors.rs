mod wasm {
    use std::{
        ffi::{c_char, c_void},
        io::{Read, Write},
    };

    #[allow(dead_code)]
    #[link(wasm_import_module = "waef.actor")]
    extern "C" {
        pub fn register_on_one_of_messages(
            message_names: *const *const c_char,
            message_names_len: u32,
            callback: extern "C" fn(*mut c_void, u32, u32) -> i32,
            context: *mut c_void,
        );

        pub fn register_on_message(
            message_name: *const c_char,
            callback: extern "C" fn(*mut c_void, u32, u32) -> i32,
            context: *mut c_void,
        );

        pub fn register_on_dispose(callback: extern "C" fn(*mut c_void), context: *mut c_void);

        pub fn dispatch(message_name: *const c_char, message_data: *const c_void, message_len: u32);

        pub fn dispatch_headered(
            message_name: *const c_char,
            header_data: *const c_void,
            header_len: u32,
            body_data: *const c_void,
            body_len: u32,
        );
    }

    pub extern "C" fn unsafe_on_message_callback<F: FnMut(&str, &[u8]) -> i32>(
        context: *mut c_void,
        name_len: u32,
        message_len: u32,
    ) -> i32 {        
        let mut stdin = std::io::stdin();
        let mut name_data = vec![0; name_len as usize];
        if name_len > 0 {
            if let Err(err) = stdin.read_exact(&mut name_data[..]) {
                _ = std::io::stderr().write_all(
                    format!("Failed to read message name for message: {err}").as_bytes(),
                );
                return 0;
            }
        }

        let name = String::from_utf8(name_data);
        if let Err(err) = name {
            _ = std::io::stderr()
                .write_all(format!("Failed to read message name for message: {err}").as_bytes());
            return 0;
        }
        let name = name.unwrap();
        
        let mut message_data = vec![0; message_len as usize];
        if message_len > 0 {
            if let Err(err) = stdin.read_exact(&mut message_data[..]) {
                _ = std::io::stderr().write_all(
                    format!("Failed to read message data for message: {err}").as_bytes(),
                );
                return 0;
            }
        }

        unsafe { (*(context as *mut F))(name.as_str(), &message_data[..]) }
    }

    pub extern "C" fn unsafe_on_dispose<F: FnMut()>(context: *mut c_void) {
        unsafe {
            let callback = context as *mut F;
            (*callback)();
        }
    }
}

use std::ffi::{c_char, c_void, CString};

pub(crate) fn register_on_one_of_messages<F: FnMut(&str, &[u8]) -> i32>(names: &[&str], callback: F) {
    unsafe {
        let names_store: Vec<CString> = names.iter().map(|n| CString::new(*n).unwrap()).collect();
        let names_arr: Vec<*const c_char> = names_store.iter().map(|n| n.as_ptr()).collect();
        let names_arr = names_arr.into_boxed_slice();

        let callback_ptr: *mut F = Box::leak(Box::new(callback));

        wasm::register_on_one_of_messages(
            names_arr.as_ptr(),
            names_arr.len() as u32,
            wasm::unsafe_on_message_callback::<F>,
            callback_ptr as *mut c_void,
        );
    }
}

#[allow(dead_code)]
pub(crate) fn register_on_message<F: FnMut(&str, &[u8]) -> i32>(name: &str, callback: F) {
    unsafe {
        let cstr_name = CString::new(name).unwrap();

        let callback_ptr: *mut F = Box::leak(Box::new(callback));

        wasm::register_on_message(
            cstr_name.as_ptr(),
            wasm::unsafe_on_message_callback::<F>,
            callback_ptr as *mut c_void,
        );
    }
}

pub(crate) fn register_on_dispose<F: FnMut()>(callback: F) {
    unsafe {
        let data: *mut F = Box::leak(Box::new(callback));

        wasm::register_on_dispose(wasm::unsafe_on_dispose::<F>, data as *mut c_void);
    }
}

pub(crate) fn dispatch(name: &str, body: &[u8]) {
    unsafe {
        let cstr_name = CString::new(name).unwrap();
        let message_data = body.as_ptr();

        wasm::dispatch(
            cstr_name.as_ptr(),
            message_data as *mut c_void,
            body.len() as u32,
        );
    }
}

#[allow(dead_code)]
pub(crate) fn dispatch_headered(name: &str, header: &[u8], body: &[u8]) {
    unsafe {
        let cstr_name = CString::new(name).unwrap();
        let header_data: *const u8 = header.as_ptr();
        let body_data: *const u8 = body.as_ptr();

        wasm::dispatch_headered(
            cstr_name.as_ptr(),
            header_data as *mut c_void,
            header.len() as u32,
            body_data as *mut c_void,
            body.len() as u32,
        );
    }
}
