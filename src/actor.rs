use std::sync::mpsc::{channel, TryRecvError};
use std::{collections::HashSet, sync::mpsc::Sender};
use std::cell::RefCell;

use waef_core::core::{IsDispatcher, IsDisposable};
use waef_core::{
    actors::{ActorsInjector, ActorsPlugin, IsActor, UseActors},
    core::{DispatchFilter, ExecutionResult, IsMessage, Message},
};

thread_local! (static POST_ON_MESSAGE: RefCell<Vec<Box<dyn FnMut(&Message)>>> = RefCell::new(vec![]));

pub fn dispatcher() -> Sender<Message> {
    let (sender, receiver) = channel();
    register_post_on_message(move |_| {
        let mut handled = 0;
        loop {
            match receiver.try_recv() {
                Ok(msg) => {
                    dispatch_message(msg);
                    handled = handled + 1;
                },
                Err(err) => {
                    match err {
                        TryRecvError::Empty => {
                            break;
                        },
                        TryRecvError::Disconnected => {
                            println!("Receiver disconnected!");
                        }
                    }
                }
            }
        }
    });
    sender
}

pub fn register_post_on_message(callback: impl FnMut(&Message) + 'static) {
    POST_ON_MESSAGE.with(|callbacks| callbacks.borrow_mut().push(Box::new(callback)));
}

pub fn register_on_message(
    filter: impl Into<DispatchFilter>,
    mut callback: impl FnMut(&Message) -> ExecutionResult,
) {
    let message_filters = match filter.into() {
        DispatchFilter::None => HashSet::new(),
        DispatchFilter::OneOf(items) => items,
        DispatchFilter::Any => HashSet::from(["".into()]),
    };
    let message_filters: Vec<&str> = message_filters.iter().map(|c| c.as_str()).collect();

    crate::bindgen::actors::register_on_one_of_messages(&message_filters[..], move |name, data| {
        let msg = Message::new_from_bytes_slice(name, data);
        let result = callback(&msg);
        POST_ON_MESSAGE.with(|callbacks| callbacks.borrow_mut().iter_mut().for_each(|cb| (cb)(&msg)));
        match result {
            ExecutionResult::NoOp => 0,
            ExecutionResult::Processed => 1,
            ExecutionResult::Kill => 2,
        }
    });
}

pub fn register_on_dispose(callback: impl FnMut()) {
    crate::bindgen::actors::register_on_dispose(callback)
}

pub fn register_boxed_actor(actor: Box<dyn IsActor>) {
    let actor = Box::leak(actor);
    register_on_message(actor.filter(), |msg| actor.dispatch(&msg));
    register_on_dispose(|| actor.dispose());
}


pub fn register_boxed_dispatcher(actor: Box<dyn IsDispatcher>) {
    let actor = Box::leak(actor);
    register_on_message(actor.filter(), |msg| actor.dispatch(&msg));
}


pub fn register_boxed_disposable(actor: Box<dyn IsDisposable>) {
    let actor = Box::leak(actor);
    register_on_dispose(|| actor.dispose());
}

pub fn register_actor(actor: impl IsActor + 'static) {
    register_boxed_actor(Box::new(actor))
}

struct WasmUseActors {}
impl UseActors for WasmUseActors {
    fn use_actor(self, actor: Box<dyn IsActor>) -> Self {
        register_boxed_actor(actor);
        self
    }
}

pub fn register_actor_plugin(plugin: impl ActorsPlugin) {
    plugin.apply(WasmUseActors {});
}

pub fn register_actor_inject(plugin: &impl ActorsInjector) {
    plugin.inject(WasmUseActors {});
}

pub fn dispatch_message(message: Message) {
    crate::bindgen::actors::dispatch(&message.name, &message.buffer);
}

pub fn dispatch(message: impl IsMessage) {
    dispatch_message(message.into_message());
}
