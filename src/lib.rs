use actor::{register_boxed_actor, register_boxed_dispatcher, register_boxed_disposable};
use waef_core::{
    actors::{ActorsInjector, ActorsPlugin, IsActor, UseActors},
    core::{IsDispatcher, IsDisposable},
    message_pod,
};

pub mod actor;
pub mod bindgen;
pub mod ecs;
pub mod resources;

pub struct WasmActor {
    disposables: Vec<Box<dyn IsDisposable>>,
    dispatchers: Vec<Box<dyn IsDispatcher>>,
    actors: Vec<Box<dyn IsActor>>,
    plugins: Vec<Box<dyn FnOnce(Self) -> Self>>,
}
impl UseActors for WasmActor {
    fn use_actor(mut self, actor: Box<dyn IsActor>) -> Self {
        self.actors.push(actor);
        self
    }
}
impl WasmActor {
    pub fn new() -> Self {
        Self {
            disposables: vec![],
            dispatchers: vec![],
            actors: vec![],
            plugins: vec![],
        }
    }

    pub fn use_disposable(mut self, disposable: impl IsDisposable + 'static) -> Self {
        self.disposables.push(Box::new(disposable));
        self
    }

    pub fn use_dispatcher(mut self, dispatcher: impl IsDispatcher + 'static) -> Self {
        self.dispatchers.push(Box::new(dispatcher));
        self
    }

    pub fn use_actor(mut self, actor: impl IsActor + 'static) -> Self {
        self.actors.push(Box::new(actor));
        self
    }

    pub fn use_plugin(mut self, plugin: impl ActorsPlugin + 'static) -> Self {
        self.plugins.push(Box::new(move |this| plugin.apply(this)));
        self
    }

    pub fn use_inject(self, plugin: &impl ActorsInjector) -> Self {
        plugin.inject(self)
    }

    pub fn start(mut self) {
        for plugin in self.plugins.drain(..).collect::<Vec<_>>() {
            self = plugin(self)
        }

        for actor in self.actors {
            register_boxed_actor(actor);
        }
        for dispatcher in self.dispatchers {
            register_boxed_dispatcher(dispatcher);
        }
        for disposable in self.disposables {
            register_boxed_disposable(disposable);
        }
    }
}

pub use ecs::WasmEcsRegistry;
pub use resources::WasmResourceStore;

#[message_pod("hot_reload:mount")]
pub struct OnHotReload {}

#[message_pod("hot_reload:unmount")]
pub struct OnHotReloadUnmount {}
